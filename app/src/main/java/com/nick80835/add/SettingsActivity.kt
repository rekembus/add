package com.nick80835.add

import android.content.Intent
import android.content.SharedPreferences
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.files.folderChooser
import com.afollestad.materialdialogs.input.input
import com.nick80835.add.databinding.SettingsLayoutBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File

class SettingsActivity : AppCompatActivity() {
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var settingsBinding: SettingsLayoutBinding
    private var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        settingsBinding = SettingsLayoutBinding.inflate(layoutInflater)
        setContentView(settingsBinding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupPreferences()
    }

    override fun onSupportNavigateUp(): Boolean {
        this.finish()
        return true
    }

    private fun setIntPref(name: String, value: Int) {
        sharedPreferences.edit().apply {
            putInt(name, value)
            apply()
        }
    }

    private fun setBoolPref(name: String, value: Boolean) {
        sharedPreferences.edit().apply {
            putBoolean(name, value)
            apply()
        }
    }

    private fun setStringPref(name: String, value: String) {
        sharedPreferences.edit().apply {
            putString(name, value)
            apply()
        }
    }

    private fun setupPreferences() {
        settingsBinding.SyncedLyricsSwitch.isChecked = sharedPreferences.getBoolean("get_synced_lyrics", false)

        settingsBinding.SyncedLyricsSwitch.setOnCheckedChangeListener { _, isChecked ->
            setBoolPref("get_synced_lyrics", isChecked)
        }

        settingsBinding.EmbeddedLyricsSwitch.isChecked = sharedPreferences.getBoolean("get_embedded_lyrics", false)

        settingsBinding.EmbeddedLyricsSwitch.setOnCheckedChangeListener { _, isChecked ->
            setBoolPref("get_embedded_lyrics", isChecked)
        }

        settingsBinding.AlbumFolderSwitch.isChecked = sharedPreferences.getBoolean("create_album_folders", true)

        settingsBinding.AlbumFolderSwitch.setOnCheckedChangeListener { _, isChecked ->
            setBoolPref("create_album_folders", isChecked)
        }

        settingsBinding.TracknameTemplateView.text = sharedPreferences.getString("trackname_template", "artist - title")

        settingsBinding.TracknameTemplateView.setOnClickListener {
            showTrackNameTemplateChooser()
        }

        settingsBinding.AlbumTracknameTemplateView.text = sharedPreferences.getString("album_trackname_template", "number - title")

        settingsBinding.AlbumTracknameTemplateView.setOnClickListener {
            showAlbumTrackNameTemplateChooser()
        }

        settingsBinding.DownloadPathView.text = sharedPreferences.getString("download_path", "${Environment.getExternalStorageDirectory().absolutePath}/Music/Deezer")

        settingsBinding.DownloadPathView.setOnClickListener {
            showDownloadPathChooser()
        }

        settingsBinding.QualityAskSwitch.isChecked = sharedPreferences.getBoolean("quality_always_ask", true)

        settingsBinding.QualityAskSwitch.setOnCheckedChangeListener { _, isChecked ->
            setBoolPref("quality_always_ask", isChecked)
        }

        settingsBinding.ThrottleSwitch.isChecked = sharedPreferences.getBoolean("throttle_downloads", false)

        settingsBinding.ThrottleSwitch.setOnCheckedChangeListener { _, isChecked ->
            setBoolPref("throttle_downloads", isChecked)
        }

        settingsBinding.TaggingSwitch.isChecked = sharedPreferences.getBoolean("tag_files", true)

        settingsBinding.TaggingSwitch.setOnCheckedChangeListener { _, isChecked ->
            setBoolPref("tag_files", isChecked)
        }

        settingsBinding.QualitySpinner.setSelection(sharedPreferences.getInt("download_quality", 1))

        settingsBinding.QualitySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                setIntPref("download_quality", position)
            }
        }

        settingsBinding.TrackPaddingSpinner.setSelection(sharedPreferences.getInt("track_number_padding", 0))

        settingsBinding.TrackPaddingSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                setIntPref("track_number_padding", position)
            }
        }

        settingsBinding.ArtworkResolutionSpinner.setSelection(sharedPreferences.getInt("preferred_art_resolution", 3))

        settingsBinding.ArtworkResolutionSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                setIntPref("preferred_art_resolution", position)
            }
        }

        settingsBinding.ArtistSepSpinner.setSelection(sharedPreferences.getInt("artist_separator", 0))

        settingsBinding.ArtistSepSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                setIntPref("artist_separator", position)
            }
        }

        settingsBinding.ResultLimitSpinner.setSelection(sharedPreferences.getInt("result_limit", 1))

        settingsBinding.ResultLimitSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                setIntPref("result_limit", position)
            }
        }

        settingsBinding.SourceButton.setOnClickListener {
            val urlIntent = Intent(Intent.ACTION_VIEW)
            urlIntent.data = Uri.parse("https://gitlab.com/Nick80835/add")
            startActivity(urlIntent)
        }

        GlobalScope.launch {
            mediaPlayer = MediaPlayer.create(this@SettingsActivity, R.raw.aeugh)
        }.invokeOnCompletion {
            runOnUiThread {
                settingsBinding.AEUGH.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorTextLight
                    )
                )

                settingsBinding.AEUGH.setOnClickListener {
                    mediaPlayer?.seekTo(0)
                    mediaPlayer?.start()
                }
            }
        }
    }

    override fun onDestroy() {
        mediaPlayer?.stop()
        mediaPlayer?.release()
        super.onDestroy()
    }

    private fun showDownloadPathChooser() {
        MaterialDialog(this).show {
            folderChooser(allowFolderCreation = true,
                context = this@SettingsActivity,
                initialDirectory = File(sharedPreferences.getString("download_path", "${Environment.getExternalStorageDirectory().absolutePath}/Music/Deezer")!!)
            ) { _, file ->
                setStringPref("download_path", file.absolutePath)
                settingsBinding.DownloadPathView.text = sharedPreferences.getString("download_path", "${Environment.getExternalStorageDirectory().absolutePath}/Music/Deezer")
            }
        }
    }

    private fun showTrackNameTemplateChooser() {
        MaterialDialog(this).show {
            title(R.string.trackname_template)
            message(text = "title, album, artist, number, contributors, date, genre, trackid")
            positiveButton(R.string.okay)
            negativeButton(R.string.cancel)

            input(prefill = sharedPreferences.getString("trackname_template", "artist - title"),
                hint = "artist - title"
            ) { _, text ->
                title = getString(R.string.trackname_template)
                setStringPref("trackname_template", text.toString())
                settingsBinding.TracknameTemplateView.text = text
            }
        }
    }

    private fun showAlbumTrackNameTemplateChooser() {
        MaterialDialog(this).show {
            title(R.string.album_trackname_template)
            message(text = "title, album, artist, number, contributors, date, genre, trackid")
            positiveButton(R.string.okay)
            negativeButton(R.string.cancel)

            input(prefill = sharedPreferences.getString("album_trackname_template", "number - title"),
                hint = "number - title"
            ) { _, text ->
                setStringPref("album_trackname_template", text.toString())
                settingsBinding.AlbumTracknameTemplateView.text = text
            }
        }
    }
}
